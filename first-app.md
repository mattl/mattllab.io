your first rails app on GitLab
==============================

TODO: expand this into a better looking, better written guide.

## You will need:

* rails + postgresql installed
* text editor
* terminal
* gitlab.com account (with SSH key added to your account)
* heroku.com account (add an app, to your project -- get the app-name (ie. foo-bar-1234) and the app-production-key (ie. 1234567890hunter21234567890) from your account settings.

* To install rails, follow the instructions at <https://rubyonrails.org>
* To install postgresql, use your OS package manager

## Getting started

* Make a new rails app: `rails _5.0.1_ new foo`
* Add a view to the application controller:
 * modify `app/controller/application_controller.rb`
 * add the following code:
 <pre>
 def hello
   render html: "Hello World".html_safe
 end</pre>
 * now make this the default route for your app:
 * modify `config/routes.rb`
 * add the following line:
 <pre>
  root 'application#hello'
 </pre>
 * finally, modify the list of required packages to support postgres
 * modify `Gemfile`
 * replace this line:
 <pre>
 require 'sqlite3'
 </pre>
 with
 <pre>
 require 'pg'
 </pre>
 * and modify `config/database.yml` and replace all the references to `sqlite3` with `pg` and change the database names from `db/database-foo` to `my-foo-database` (ie. `db/database-test` becomes `my-test-database`)
 * Okay, run `bundle install` to make sure this all works locally.
 * Run the local rails server by typing `rails s` and check out the provided URL in your browser. you should see "hello world"

## Checking it all into GitLab

* Go to <https://gitlab.com> and sign in.
* Make a new project
* Follow the provided directions to check your local project into gitlab.
* From the project homepage, click "Set up CI" and add the 'Ruby' template from the dropdown.
* Add your HEROKU_APP_NAME and HEROKU_PRODUCTION_KEY variables to your project


 
